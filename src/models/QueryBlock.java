/**
 * 
 */
package models;

import java.util.LinkedHashMap;


/**
 * @author vaibhavsaini
 *
 */
public class QueryBlock extends LinkedHashMap<String, Integer> {
    private String id;
    private int size;

    /**
     * @param id
     */
    public QueryBlock(String id) {
        super();
        this.id = id;
        this.size=0;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    public int getSize() {
        if(this.size == 0){
            for (Integer freq : this.values()) {
                this.size += freq;
            }
        }
        return this.size;
    }
}
